# [1.6.0](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.5.5...v1.6.0) (2021-04-08)


### Bug Fixes

* change title ([45add0d](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/45add0db449aecd2509ff0331faf82cb440ac79b))
* fix rubocop errors ([449b4f5](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/449b4f53b54897ff6e70b6980fe6414213f83ab9))
* optionally start sentry ([755ad1a](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/755ad1a7ba5f83b773c05c0202369ab4a3676d56))
* update date column on artwork ([9a52a2e](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/9a52a2e9daa864f02d48b9171e020ecf5907f7f9))
* update dev dependencies ([848ae66](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/848ae66e0837642e73b73be3e6b6f29ff487a00a))
* update header img placeholder ([34ff6be](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/34ff6be154bb32476e33b27836948c4fd45314d8)), closes [#2](https://gitlab.com/urbanlink/kunst-openbare-ruimte/issues/2)
* update pagination ([aaa37ff](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/aaa37ff3fa04b8fe14eb71388add0333f714031b))


### Features

* add gravatar to user ([89de100](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/89de100d277a724e75b655cc5ed736489cee31b1))

## [1.5.5](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.5.4...v1.5.5) (2021-04-08)


### Bug Fixes

* move tailwinfd components for purgingh ([f8cefc7](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/f8cefc7670839e2b9fb7606045816268caf9a08d))
* update action  mailer config ([0518967](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/05189674435d8f043c1e7dd846a505459d13cbc6))

## [1.5.4](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.5.3...v1.5.4) (2021-04-08)


### Bug Fixes

* add gemfile for sentry ([883dda5](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/883dda593c6d11ac579b746800f51be59d27e886))
* add Sentry error reporting ([2a5d8f5](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/2a5d8f52a02dc4fe3f0b904f3e997321303e5da0)), closes [#19](https://gitlab.com/urbanlink/kunst-openbare-ruimte/issues/19)
* remove sentry environment ([fb4f540](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/fb4f54025b72e8268a38362d0864bfd355b59685))

## [1.5.3](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.5.2...v1.5.3) (2021-04-07)


### Bug Fixes

* reinstate devise ([60143b6](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/60143b6ea99addb272002fa27a5cc0cef54a1541))

## [1.5.2](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.5.1...v1.5.2) (2021-04-07)


### Bug Fixes

* disable devise whitelist ([696ede2](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/696ede298d1493cf21aaf9428aa1eac43e9457d2))
* remove simple_form ([575698b](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/575698b0a101c3de99ac0a5e0fcc12a88dd29e3a))

## [1.5.1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.5.0...v1.5.1) (2021-04-07)


### Bug Fixes

* check image_tag ([f57021f](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/f57021fc84ff8e0905d5e528c37f8c50fd815d95))
* update devise whitelist ([d8ff9e6](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/d8ff9e6a959e028d626fd504f42f3b8b5aaccc48))

# [1.5.0](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.4.1...v1.5.0) (2021-04-06)


### Bug Fixes

* add better errors during development ([ae9b384](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/ae9b3840d5c9df65ca94a4e08b366ef798de4512))
* update comments ([ab0d339](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/ab0d3390f733da70dfb7ba764f96e3d8f7f57084))


### Features

* add pages route ([85ad335](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/85ad3358c5028d95049ec0d3fdd37f17239a827b))

## [1.4.1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.4.0...v1.4.1) (2021-03-30)


### Bug Fixes

* fix code error ([6b4c825](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/6b4c8252f2ae9831267cc8175b5e7f57119a42e1))

# [1.4.0](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.3.0...v1.4.0) (2021-03-29)


### Bug Fixes

* remove rails-admin ([5ad6224](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/5ad62248dc15aa72ddd09af87c16b545444d0b78))


### Features

* add custom admin dashboard ([82a8caf](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/82a8cafd5cd6a2cf43d87cff09d06567d0470175))
* add registration options ([1585ad2](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/1585ad24cda4cd04ce11022e362157b2e7485072))

# [1.3.0](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.2.1...v1.3.0) (2021-03-28)


### Bug Fixes

* add commitlint ([0ee3e70](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/0ee3e7073c7a00523eac21a930bb9e0565606090))


### Features

* add cancancan and user emails ([f979ce4](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/f979ce493d5959489c96d366c279aa189ff0989c))
* add devise user authentication ([797f1e5](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/797f1e556ed145b80b50b061e13677db8a8815ee))

## [1.2.1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.2.0...v1.2.1) (2021-03-27)


### Bug Fixes

* add commitlint ([209d85f](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/209d85fac40e35e29b8c43404735b1c1bbd2024e))

# [1.2.0](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.9...v1.2.0) (2021-03-27)


### Bug Fixes

* **ci:** update semantic release ([75a11f1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/75a11f1a0790a3c9dad0f7bed7feb271dd225493))
* add new data ([fbd216b](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/fbd216b1b196f96b58c90cc92458b8ceae5d8ced))
* add semantic release to dependencies ([b32439a](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/b32439a7af5b73353ef1574a493036550baf67d2))
* **ci:** update stage order ([951560c](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/951560cd10ad613ee8d657dee6d1d176004888ba))
* add semantic release ([17b23e1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/17b23e10d6599a3cd58e4e9d6ad0e637ffb45ae8))
* update ci config ([f6320d1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/f6320d1af3c16ee2356e250a82b9870e6301ec2b))
* update gitlab-ci ([5ec6c70](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/5ec6c7079185521784f9d314d8cc24e8bac2d262))
* update header footer and about ([6e81398](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/6e8139883480dcce7c2179cbac39d2db3e441aeb))
* update image component ([e0fdedf](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/e0fdedf7b3fbcd9dfd79c60d1252f0b4206e6930))
* update image plugin ([792d65b](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/792d65b41da0862c38b640b4796e4d69cdf812ed))
* update images ([36433b6](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/36433b6476c74fd00bfd15b8184ccca653bc7935))
* update packages ([6a91627](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/6a916277c1c4a664c453177142bcc9cea3056082))
* update packages ([3bb8b7e](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/3bb8b7efca3cbcf27b18c434054fc650b04bee41))
* update scraper ([a2f4b56](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/a2f4b56808feeaafd13a67210287e913208685d2))


### Features

* move to ruby-on-rails ([b12489d](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/b12489dc619f094f53fc9be0a15f128af9668c48))

## [1.1.9](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.8...v1.1.9) (2021-03-08)


### Bug Fixes

* update templates ([ee6e5f1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/ee6e5f11f6b980dec175ff90f3087ac100d5618f))

## [1.1.8](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.7...v1.1.8) (2021-03-07)


### Bug Fixes

* add link to list ([8df4044](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/8df404423b1cd8c1ebc0cc367208b67e604075a7))

## [1.1.7](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.6...v1.1.7) (2021-03-07)


### Bug Fixes

* **ci:** add artifacts to pages ([54ff143](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/54ff143ba3dfe228cbffe72f70ffd454e4f404ab))

## [1.1.6](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.5...v1.1.6) (2021-03-07)


### Bug Fixes

* add build cache to pages ([1669a4b](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/1669a4bc76e7bad4a2c86bbde480f91e2fc6c3f8))
* update ci ([ac84ad7](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/ac84ad741f15c317b0bfc37a2d64640daaa8b6e8))
* update ci jobs ([8f2242a](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/8f2242a17480824f182df6e5714582db456876bc))

## [1.1.5](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.4...v1.1.5) (2021-03-07)


### Bug Fixes

* update ci ([d71502a](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/d71502a255dd41316a125500c20812d3ccd82e8d))
* **ci:** update build step ([e303ddd](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/e303ddddb671ec0a5578778fe354097e8e02c4ca))

## [1.1.4](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.3...v1.1.4) (2021-03-07)


### Bug Fixes

* **ci:** test ci ([c5b6ce4](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/c5b6ce4f3743b52c45b94f4b174478573eac27fa))

## [1.1.3](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.2...v1.1.3) (2021-03-07)


### Bug Fixes

* ci ([3f14bad](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/3f14bad8c3cce88d64adfe0a480c5d321a22a1b5))
* **ci:** try pages ([a5a5093](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/a5a5093a78f3b0448c8e89d8f769009ad267b633))

## [1.1.2](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.1...v1.1.2) (2021-03-07)


### Bug Fixes

* **ci:** add artifacts to pages ([07a0803](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/07a0803d79e6456f9b57b5ddc85d6bfe7d3d8d10))

## [1.1.1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.1.0...v1.1.1) (2021-03-07)


### Bug Fixes

* **ci:** add cache to release ([675937a](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/675937a89f9c8e7ce920c6e74d7eced22a8dc381))
* update ci config ([2765685](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/2765685d563c8869a0bd389b73605f56f2d2b48b))
* update CI pages ([255132e](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/255132e48eeb6125ae5fc1b5d6df5565cacbbed8))

# [1.1.0](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.0.2...v1.1.0) (2021-03-07)


### Features

* add list and artwork pages ([7436bce](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/7436bce30f95e7d7cbfb9f0f62d2491b7eb20e69))

## [1.0.2](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.0.1...v1.0.2) (2021-03-06)


### Bug Fixes

* update linting ([76c0885](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/76c0885f92bc385148f2a1a307bd9b64b1299595))

## [1.0.1](https://gitlab.com/urbanlink/kunst-openbare-ruimte/compare/v1.0.0...v1.0.1) (2021-03-06)


### Bug Fixes

* update gitlab-ci ([26a652d](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/26a652d33e7b7a18cb575975a2611872ea089450))

# 1.0.0 (2021-03-06)


### Bug Fixes

* add commitlint ([1303275](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/1303275608ae2215fa6fde21a91d203e08f6a1af))
* publish with gitlab ([9ba46ed](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/9ba46ed402652b3d9cbf0e44c2f60d7d9ec376bc))
* sr ([7913296](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/7913296f53f19280835fd74c918eb8862f574bfe))
* update ci ([2fda050](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/2fda0506a38d424c0dfce7a5a721f7b2e13b936e))
* update config ([aaa20f8](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/aaa20f867c3bbb48a8bfa99b16a9e10a74a3cb36))
* update sr ([2fda23f](https://gitlab.com/urbanlink/kunst-openbare-ruimte/commit/2fda23ff8789930aaf75e48881be5a64ed31c400))
