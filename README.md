# Kunst openbare Ruimte


  1. ~~Source data from JSON (fetch remote images)~~
  2. ~~Seed data to database~~
  3. ~~Encrypt vars or env vars in docker?~~
  4. ~~Create docker containter for production~~
  5. ~~Add container to gitlab container registry~~
  6. ~~Publish in digital ocean container ~~
  7. Create users (register, login, logout)
  8. Create comments for artworks
  9.

## Users
  * Set default admin user during seed
  * User authentication using Devise
  * Admin approval required
    Reference: https://github.com/heartcombo/devise/wiki/How-To:-Require-admin-to-activate-account-before-sign_in
  * User roles: https://altalogy.com/blog/rails-6-user-accounts-with-3-types-of-roles/

## References

### Active Storage

  * https://levelup.gitconnected.com/rails-image-upload-101-f9bf245e389b
  * https://min.io/
  * https://kevinjalbert.com/rails-activestorage-configuration-for-minio/
  *

### Docker

  * https://github.com/ledermann/docker-rails
  * https://blog.wildcat.io/2019/06/rails-with-docker-part-1/
  *
