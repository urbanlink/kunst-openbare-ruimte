module.exports = {
  purge: [
    "./app/**/*.html",
    "./app/**/*.erb",
    "./app/**/*.vue",
    "./app/**/*.jsx",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true,
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
