class CommentsController < ApplicationController
  before_action :set_artwork
  before_action :set_comment, only: [:destroy]

  def create
    @comment = @artwork.comments.create(comment_params)
    @comment.user_id = current_user.id
    @comment.artwork_id = params[:artwork_id]
    @comment.save

    redirect_to artwork_path(@artwork)

  end

  def destroy
    @comment.destroy
    redirect_to article_path(@article)
  end

  private

    def set_artwork
      @artwork = Artwork.find(params[:artwork_id])
    end

    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:body)
    end
end
