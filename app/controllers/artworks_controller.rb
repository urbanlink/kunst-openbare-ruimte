
class ArtworksController < ApplicationController
  before_action :set_artwork, only: [:show, :edit, :update, :show, :get_comments]
  # Guest users can only view artworks
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @artworks = Artwork.paginate(page: params[:page], per_page: 50)
  end

  def show
  end

  def new
    @artwork = Artwork.new
  end

  def create
    @artwork = Artwork.new(artwork_params)

    if @artwork.save
      redirect_to @artwork, notice: "Blog was successfully created."
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @artwork.update(artwork_params)
      redirect_to @artwork
    else
      render :edit
    end
  end

  # DELETE /artworks/1
  def destroy
    @artwork.destroy

    redirect_to root_path, notice: "Artwork was successfully destroyed"
  end

  private

  def artwork_params
    params.require(:artwork).permit(:title, :description)
  end

  def set_artwork
    @artwork = Artwork.find(params[:id])
  end

  def get_comments
    comments = @artwork.comments.select("comments.*", users.display_name).joins(":user").by_created_at
    render json: {comments: comments }
  end
end
