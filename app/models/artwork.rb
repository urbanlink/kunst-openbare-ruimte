class Artwork < ApplicationRecord
  has_one_attached :featured_img do |attachable|
    attachable.variant :thumb, resize: "100x100"
  end

  # dependent: :destroy means the comments related
  # to the specific post in mention get deleted if the post does.
  has_many :comments, dependent: :destroy

  validates :city, presence: true
  validates :title, presence: true
  validates :description, presence: true, length: { minimum: 10 }
  # featured_img: nil,
  # neighbourhood:
  # artist:
  # copyright_img:
  # materials:
  # source_id:
  # date_placement:
  # date_bought:
  # copyright_description:
end
