class Comment < ApplicationRecord
  belongs_to :artwork
  belongs_to :user

  def self.by_created_at
    order("created_at DESC")
  end
end
