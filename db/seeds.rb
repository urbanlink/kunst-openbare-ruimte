require 'json'

puts "Removing existing data from db"
User.delete_all
Comment.delete_all
Artwork.delete_all

# Create admin user
User.create(email: ENV["ADMIN_EMAIL"], password: ENV["AMDIN_PASSWORD"])

puts "Load source json"
file = open("data/seed_data/denhaag.json")
json = file.read
parsed = JSON.parse(json)

data=[]

puts "Proces lines"
parsed.each do |artwork|

  # Album.create(title: 'Delicate Roses', location: "Chamber's Rose Garden", description: 'A collection of gorgeous roses', date: Date.new(2017, 5, 25), photographer_id: 3, client_id: 4)
  # Photo.create(title: 'Under the Leaves', description: 'Bailey hangs out by a plant on the bed', date: Date.new(2019, 6, 18), album_id: 5).image.attach(io: File.open('app/assets/images/seed_images/dogs/dog_7.jpg'), filename: 'dog_7.jpg')

  puts "."
  a = Artwork.new

  a.city = "Den Haag"
  a.title = artwork["title"]
  a.description = artwork["description"]
  a.neighbourhood = artwork["neighbourhood"]
  a.copyright_img = artwork["copyright_img"]
  a.copyright_description = artwork["copyright_description"]
  a.artist = artwork["artist"]
  a.materials = artwork["materials"]
  a.source_id = artwork["source_id"]
  a.date_placement = artwork["date_placement"]
  a.date_bought = artwork["date_bought"]

  if artwork["featured_img"]
    a.featured_img.attach(
      io: File.open("data/seed_images/denhaag/#{artwork["featured_img"]}"),
      filename: artwork["featured_img"]
    )
  end

  a.save
end
