class UpdateFieldsForComments < ActiveRecord::Migration[6.1]
  def change
    remove_column :comments, :commenter
    remove_column :comments, :status
  end
end
