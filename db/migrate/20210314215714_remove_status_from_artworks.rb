class RemoveStatusFromArtworks < ActiveRecord::Migration[6.1]
  def change
    remove_column :artworks, :status, :string
  end
end
