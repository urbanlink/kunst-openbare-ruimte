class UpdateDatefieldForArtworks < ActiveRecord::Migration[6.1]
  def change
    change_column :artworks, :date_placement, :datetime, using: 'date_placement::timestamp without time zone'
  end

  def down
    change_column :artworks, :date_placement, :string
  end
end
