class AddCopyrightToArtwork < ActiveRecord::Migration[6.1]
  def change
    add_column :artworks, :copyright_description, :string
  end
end
