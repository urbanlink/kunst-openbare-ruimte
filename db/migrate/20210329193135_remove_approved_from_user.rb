class RemoveApprovedFromUser < ActiveRecord::Migration[6.1]
  def change
    remove_index  :users, :approved
    remove_column :users, :approved
  end
end
