class RemoveImageFromArtwork < ActiveRecord::Migration[6.1]
  def change
    remove_column :artworks, :featured_img, :string
  end
end
