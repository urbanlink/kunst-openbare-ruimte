class AddFieldsToArtworks < ActiveRecord::Migration[6.1]
  def change
    add_column :artworks, :city, :string
    add_column :artworks, :neighbourhood, :string
    add_column :artworks, :artist, :string
    add_column :artworks, :copyright_img, :string
    add_column :artworks, :materials, :string
    add_column :artworks, :source_id, :string
    add_column :artworks, :date_placement, :string
    add_column :artworks, :date_bought, :datetime
  end
end
