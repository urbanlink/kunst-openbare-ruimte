class AddStatusToArtworks < ActiveRecord::Migration[6.1]
  def change
    add_column :artworks, :status, :string
  end
end
