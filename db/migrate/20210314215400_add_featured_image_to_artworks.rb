class AddFeaturedImageToArtworks < ActiveRecord::Migration[6.1]
  def change
    add_column :artworks, :featured_img, :string
  end
end
