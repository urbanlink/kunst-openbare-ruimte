# -----------------------------------
# Source data 
#
# Convert data to standardized format 
# Download remote images
#
# -----------------------------------

require "down"
require 'json'
require "uuidtools"

# Constants 
OWNER = "denhaag"
SOURCE_DATA_DIR = "data/seed_data"
SOURCE_DATA_NAME = "src/kunstobjecten-#{OWNER}.json"

OUTPUT_DATA_DIR = "data/seed_data/#{OWNER}"
OUTPUT_DATA_NAME = "#{OWNER}.json"
OUTPUT_IMG_DIR = "data/seed_images/#{OWNER}"

#####
#
# Fetch remote file and return new random filename 
#
#####
def fetch_image(url)
  # Download file to virtual tempfile
  tempfile = Down.download(url)
  # Extract extension from downloaded file
  ext = File.extname(tempfile.original_filename)
  # Create a unique filenama
  name = UUIDTools::UUID.random_create
  # Combine filename and extension
  filename = "#{name}#{ext}"
  # Save file to the directeory in the filesystem 
  FileUtils.mv(tempfile.path, "#{OUTPUT_IMG_DIR}/#{filename}")

  return filename
end

#####
#
# Load file and parse each line/object
#
#####
def parse_data 
  # Read file per object 
  puts "Load and parse source json "
  file = open("#{SOURCE_DATA_DIR}/#{SOURCE_DATA_NAME}")
  json = file.read 
  parsed = JSON.parse(json)

  data=[]
  
  # Rename and restructure 
  parsed["features"].each do |artwork|
    # Fetch Featured image and save new filename 
    if (artwork["properties"]["OVERZICHTSFOTO"])
      begin
        new_image_filename = fetch_image(artwork["properties"]["OVERZICHTSFOTO"])
      rescue Down::ResponseError
        puts "FILE NOT FOUND"
        File.write("#{OUTPUT_DATA_DIR}/error.log", "#{Time.now.to_i},404, File not found, #{artwork["properties"]["ID"]} \r\n", mode: 'a')
      end
    end

    object = {
      "city": "Den Haag",
      "neighbourhood": artwork["properties"]["BUURT_IMGEO_OMSC"],
      "title": artwork["properties"]["TITEL_KUNSTOBJECT"],
      "description": artwork["properties"]["OMSCHRIJVING_KUNSTOBJECT"] || 'No description given',
      "featured_img": new_image_filename,
      "copyright_img": artwork["properties"]["COPYRIGHT_OVERZICHT_FOTO"],
      "copyright_description": artwork["properties"]["COPYRIGHT_OMSCHRIJVING"],
      "artist": artwork["properties"]["KUNSTENAAR"],
      "materials": artwork["properties"]["MATERIALEN_OMSC"],
      "source_id": artwork["properties"]["ID"],
      "date_placement": artwork["properties"]["PLAATSINGSDATUM"],
      "date_bought": artwork["properties"]["DATUM_AANSCHAF"]
    }
    data << object
  end

  # Save entry in output file 
  File.write("#{OUTPUT_DATA_DIR}/#{OUTPUT_DATA_NAME}", JSON.dump(data))
end

# Clear tmp data

# Create directories
def create_directories 
  # Delete folders
  FileUtils.rm_rf OUTPUT_DATA_DIR
  FileUtils.rm_rf OUTPUT_IMG_DIR
  # Create folders
  FileUtils.makedirs OUTPUT_DATA_DIR + "/log"
  FileUtils.makedirs OUTPUT_IMG_DIR
  # Create json file (to be used by seed.rb)
  File.open("#{OUTPUT_DATA_DIR}/#{OUTPUT_DATA_NAME}", 'w')
  # Create error logfile
  File.open("#{OUTPUT_DATA_DIR}/error.log", 'w')
  File.write("#{OUTPUT_DATA_DIR}/error.log", "Timestamp, Code, Error, Object ID \r\n", mode: 'a')
end

# Start
create_directories
parse_data


# create_img_derivatives
puts '*** DONE ***'