# FROM ledermann/rails-base-builder:3.0.0-alpine as Builder
# Step 1 Build 
FROM ruby:3.0.0-alpine as builder
RUN apk add --no-cache \
  build-base \
  postgresql-dev \
  git \
  nodejs \
  yarn \
  tzdata \
  file \
  shared-mime-info

# Set application directory 
WORKDIR /app

# Install standard node modules 
COPY package.json yarn.lock /app/
RUN yarn install --frozen-lockfile 

# Install standard gems
COPY Gemfile* /app/ 

RUN bundle config --local without 'development test' && \
  bundle install -j4 --retry 3 && \
  # Remove unneeded gems 
  bundle clean --force && \
  # Remove unneeded files from installed gems 
  rm -rf /usr/local/bundle/cache/*.gem && \
  find /usr/local/bundle/gems/ -name "*.c" -delete && \
  find /usr/local/bundle/gems/ -name "*.o" -delete

COPY . /app 

RUN mv config/credentials.yml.enc config/credentials.yml.enc.bak 2>/dev/null || true
RUN mv config/credentials config/credentials.bak 2>/dev/null || true 
RUN --mount=type=secret,id=npmrc,dst=/root/.npmrc \
  --mount=type=secret,id=yarnrc,dst=/root/.yarnrc.yml \
  RAILS_ENV=production \
  SECRET_KEY_BASE=dummy \
  RAILS_MASTER_KEY=dummy \
  bundle exec rails assets:precompile
RUN mv config/credentials.yml.enc.bak config/credentials.yml.enc 2>/dev/null || true
RUN mv config/credentials.bak config/credentials 2>/dev/null || true

# Remove folders not needed in resulting image
RUN rm -rf node_modules \
  tmp/cache \
  vendor/bundle \
  test \
  spec \ 
  app/javascript \
  app/frontend \
  app/packs

# Remove some files not needed in resulting image
RUN rm .browserslistrc \ 
  babel.config.js \
  package.json \
  postcss.config.js \
  yarn.lock

####################
# Step 2 Final image
# FROM ledermann/rails-base-final:3.0.0-alpine
FROM ruby:3.0.0-alpine 
RUN apk add --no-cache \
  postgresql-client \
  tzdata \
  file 

# Configure rails 
# This image is for production env only
ENV RAILS_LOG_TO_STDOUT true 
ENV RAILS_SERVE_STATIC_FILES true 
ENV RAILS_ENV production

WORKDIR /app 

EXPOSE 3000

# Write GIT commit SHA and TIME to env vars
ARG COMMIT_SHA
ARG COMMIT_TIME

ENV COMMIT_SHA ${COMMIT_SHA}
ENV COMMIT_TIME ${COMMIT_TIME}

# Add user
RUN addgroup -g 1000 -S app && \
  adduser -u 1000 -S app -G app

# Copy app with gems from former build stage
COPY --from=builder --chown=app:app /usr/local/bundle/ /usr/local/bundle/
COPY --from=builder --chown=app:app /app /app

# Add Alpine packages
RUN apk add --no-cache imagemagick

# Switch to application use
USER app

# Start up
CMD ["docker/startup.sh"]
