Rails.application.routes.draw do
  root "artworks#index"

  devise_for :users, path: "", path_names: { sign_in: "login", sign_out: "logout", sign_up: "register" }

  get 'pages/about'

  resources :artworks do
    resources :comments, only: [:create]
    member do
      get :get_comments
    end
  end

  namespace :admin do
    get 'dashboard/index'
  end
end
